//Laser gate chronograph, with time to serial output.
/*
 * PIR sensor tester
 */
// macros from DateTime.h 
/* Useful Constants */
#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)  
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN) 
#define numberOfHours(_time_) (( _time_% SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) ( _time_ / SECS_PER_DAY)  

//Each sensor is defined here in consecutive order from bottom to top.
int sensor1= 2;
int sensor2= 3;
int sensor3= 4;
int sensor4= 5;
int sensor5= 6;

int val = 0;

void setup() {

 pinMode(sensor1, INPUT);
 pinMode(sensor2, INPUT);
 pinMode(sensor3, INPUT);
 pinMode(sensor4, INPUT);
 pinMode(sensor5, INPUT);
 // initialize serial communications at 9600 bps:
 Serial.begin(9600);
}

void loop(){ 

  //If two consecutive beams are broken. which will decrease the light intensity below 
  //threshhold making sensor = LOW. print time in "hours:minutes:seconds"

  if((digitalRead(sensor1) == LOW)and(digitalRead(sensor2) == LOW)) {
      delay (1000);//delay added to reduce false goals, inomilies in sensor state
    //  Serial.println("sense1");//individual print to monitor which beam was broke
    //  Serial.println("Motion detected!");
    //  Serial.println("Time: ");
      time(millis() / 1000); //millis is time since sketch started running
  }else if((digitalRead(sensor2) == LOW)and(digitalRead(sensor3) == LOW) ){
      delay (1000);//delay added to reduce false goals, inomilies in sensor state
      Serial.println("sense2");// individual print to monitor which beam was broke
     // Serial.println("Motion detected!");
      //Serial.println("Time: ");
      time(millis() / 1000);//millis is time since sketch started running
  }else if((digitalRead(sensor3) == LOW)and(digitalRead(sensor4) == LOW) ){
      delay (1000);//delay added to reduce false goals, inomilies in sensor state
     // Serial.println("sense3");// individual print to monitor which beam was broke
     // Serial.println("Motion detected!");
     // Serial.println("Time: ");
      time(millis() / 1000);//millis is time since sketch started running
  }else if((digitalRead(sensor4) == LOW)and(digitalRead(sensor5) == LOW) ){
      delay (1000); //delay added to reduce false goals, inomilies in sensor state
     // Serial.println("sense4");// individual print to monitor which beam was broke
     // Serial.println("Motion detected!");
     // Serial.println("Time: ");
      time(millis() / 1000);//millis is time since sketch started running
  }
}


void time(long val){  
   //Number of days is irrelevant as football matches typically last 1 hour
//int days = elapsedDays(val);
int hours = numberOfHours(val);
int minutes = numberOfMinutes(val);
int seconds = numberOfSeconds(val);

 // digital clock display of current time, set to contain hours, minutes and seconds
 Serial.print(hours,DEC);  
 //printDigits(hours);  
 printDigits(minutes);
 printDigits(seconds);
 Serial.println();  
 
}

void printDigits(byte digits){
 // utility function for digital clock display: prints colon and leading 0
 Serial.print(":");
 if(digits < 10)
   Serial.print('0');
 Serial.print(digits,DEC);  
}